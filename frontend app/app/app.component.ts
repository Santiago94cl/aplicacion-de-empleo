import {Component} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/toPromise'; 
import {DataService} from './data.service';
 


@Component({
  selector: 'app',
  templateUrl: 'app/views/app.component.html'
})
export class AppComponent {

  public nombre : string = 'santiago';
    public apellido : string = 'cubillos';
    public empleo : number = 2;
    public experiencia : number = 2;
    data : any = {

        nombre: this.nombre,
        apellido: this.apellido,
        empleo: this.empleo,
        experiencia: this.experiencia

      }
    url = 'http://httpbin.org/post';
    

  constructor(private http :Http){


    this.http.post(this.url, this.data)
            .toPromise()
            .then((data:any) => {console.log('datos enviados')})
    
  }

}